# eccs-maps

`maps.cs.earlham.edu`

## Tiles and Downloads
Tiles and downloads should be kept separate, since they are large files.
a `tiles/` folder and `downloads/` folder should be added to this repository in place.
